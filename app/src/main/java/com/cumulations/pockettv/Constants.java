package com.cumulations.pockettv;

import org.w3c.dom.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Amit Tumkur on 23-04-2018.
 */

public class Constants {
    private static List<String> tabsNames;
    private static HashMap<String,String> tvNameUrlsMap;
    private static List<String> tvList;

    public static List<String> getTabsNames(){
        if (tabsNames == null) {
            tabsNames = new ArrayList<>();
            tabsNames.add("TV Shows");
            tabsNames.add("Comedy");
            tabsNames.add("Sports");
            tabsNames.add("News");
            tabsNames.add("Movies");
            tabsNames.add("Trending");
            tabsNames.add("Music");
        }

        return tabsNames;
    }

    public static HashMap<String,String> getTvNameUrlsMap(){
        if (tvNameUrlsMap == null){
            tvNameUrlsMap = new HashMap<>();
            tvNameUrlsMap.put("Hotstar","http://www.hotstar.com/");
            tvNameUrlsMap.put("SonyLIV","http://www.sonyliv.com/");
            tvNameUrlsMap.put("YuppTV","http://www.yupptv.com/");
            tvNameUrlsMap.put("Voot","http://www.voot.com/");
            tvNameUrlsMap.put("nexGTv","http://m.nexgtv.com/");
            tvNameUrlsMap.put("Viki","http://www.viki.com/");
        }

        return tvNameUrlsMap;
    }

    public static List<String> getTvList(){
        if (tvList == null) {
            tvList = new ArrayList<>();
            for (Map.Entry<String,String> tvNameUrl : getTvNameUrlsMap().entrySet()){
                tvList.add(tvNameUrl.getKey());
            }
        }

        return tvList;
    }
}
