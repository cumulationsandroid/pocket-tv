package com.cumulations.pockettv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cumulations.shopping.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Amit Tumkur on 19-12-2017.
 */

public class GridRecyclerAdapter extends RecyclerView.Adapter<GridRecyclerAdapter.MyViewHolder> {

    private Context context;
    private List<String> tvList;
    private HashMap<String,String> tvNameUrls;

    public GridRecyclerAdapter(Context context, HashMap<String, String> tvNameUrls) {
        this.context = context;
        this.tvNameUrls = tvNameUrls;
    }

    public GridRecyclerAdapter(Context context, List<String> tvList) {
        this.context = context;
        this.tvList = tvList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView tvImage;
        LinearLayout placeItemLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvImage = itemView.findViewById(R.id.tvImage);
            placeItemLayout = itemView.findViewById(R.id.gridItemLayout);
        }
    }

    @Override
    public GridRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item, parent, false);
        return new GridRecyclerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GridRecyclerAdapter.MyViewHolder myViewHolder, final int position) {
        myViewHolder.tvName.setText(tvList.get(position));

        myViewHolder.placeItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)context).launchChromeTab(tvList.get(myViewHolder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return tvList != null ? tvList.size() : 0;
//        return tvNameUrls != null ? tvNameUrls.size() : 0;
    }

}
