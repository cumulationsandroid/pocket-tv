package com.cumulations.pockettv;

import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.cumulations.shopping.R;
import com.flyco.pageindicator.indicator.FlycoPageIndicaor;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private SlidingTabsViewPagerAdapter mSlidingTabsViewPagerAdapter;
    private ViewPager mTabsViewPager;
    private PicsFragAdapter adapter;
    private ViewPager imagesViewPager;
    private View decorView;
    private FlycoPageIndicaor indicator;
    private DrawerLayout drawerLayout;
    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private String lastState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        decorView = getWindow().getDecorView();
        drawerLayout = findViewById(R.id.drawer_layout);

        mSlidingTabsViewPagerAdapter = new SlidingTabsViewPagerAdapter(getSupportFragmentManager());
        mTabsViewPager = findViewById(R.id.tabsViewpager);
        mTabsViewPager.setAdapter(mSlidingTabsViewPagerAdapter);

        appBarLayout = findViewById(R.id.appbar);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mTabsViewPager);

        imagesViewPager = (ViewPager) findViewById(R.id.imagesViewPager);
        adapter = new PicsFragAdapter(getSupportFragmentManager());
        indicator = ViewFindUtils.find(decorView, R.id.circlePageIndicator);

        List<Integer> bannerIds = new ArrayList<>();
        bannerIds.add(R.drawable.hotstar1);
        bannerIds.add(R.drawable.hotstar2);
        bannerIds.add(R.drawable.hotstar3);
        bannerIds.add(R.drawable.hotstar4);
        bannerIds.add(R.drawable.sony1);
        bannerIds.add(R.drawable.sony2);
        bannerIds.add(R.drawable.sony3);
        adapter.setSource(bannerIds);
        imagesViewPager.setAdapter(adapter);
        indicator.setViewPager(imagesViewPager, bannerIds.size());

        findViewById(R.id.hamburgerBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if (verticalOffset == 0 || verticalOffset <= toolbar.getHeight()) {
                if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    Log.d("OnOffsetChangedListener", "collapsed");
                    if (lastState!=null && lastState.equals("collapsed")) {
                        appBarLayout.addOnOffsetChangedListener(null);
                    } else {
                        lastState = "collapsed";
                        updateTabColors("collapsed");
                    }
                } else if (verticalOffset == 0) {
                    Log.d("OnOffsetChangedListener", "expanded");
                    if (lastState!=null && lastState.equals("expanded")) {
                        appBarLayout.addOnOffsetChangedListener(null);
                    } else {
                        lastState = "expanded";
                        updateTabColors("expanded");
                    }
                }

            }
        });

    }

    private void updateTabColors(String state) {
        if (state.equals("collapsed")){
            appBarLayout.setBackgroundColor(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimary));
                tabLayout.setTabTextColors(ContextCompat.getColor(HomeActivity.this, android.R.color.white),
                        ContextCompat.getColor(HomeActivity.this, android.R.color.white));
                tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(HomeActivity.this, android.R.color.white));
        } else {
            appBarLayout.setBackgroundColor(ContextCompat.getColor(HomeActivity.this, android.R.color.white));
            tabLayout.setTabTextColors(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimary),
                    ContextCompat.getColor(HomeActivity.this, R.color.colorPrimary));
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimary));
        }
    }


    public class SlidingTabsViewPagerAdapter extends FragmentPagerAdapter {

        public SlidingTabsViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return new TabFragment();
        }

        @Override
        public int getCount() {
            return Constants.getTabsNames().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Constants.getTabsNames().get(position);
        }
    }

    class PicsFragAdapter extends FragmentStatePagerAdapter {

        List<Integer> pics;

        public PicsFragAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return PicsPagerFragItem.loadPic(pics.get(i));
        }

        @Override
        public int getCount() {
            return pics.size();
        }


        public void setSource(List<Integer> pics) {
            this.pics = pics;
        }
    }

    public void launchChromeTab(String key){
        String url = Constants.getTvNameUrlsMap().get(key);
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(this,R.color.colorPrimary));
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }
}
