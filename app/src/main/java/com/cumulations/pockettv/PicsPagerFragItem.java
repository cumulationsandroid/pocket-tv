package com.cumulations.pockettv;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cumulations.shopping.R;

/**
 * Created by amit_ on 29-04-2016.
 */
public class PicsPagerFragItem extends Fragment {
    public static final String PIC = "pic";
    private int picId;

    public PicsPagerFragItem() {
    }

    public static PicsPagerFragItem loadPic(int pic) {
        PicsPagerFragItem fragment = new PicsPagerFragItem();
        Bundle args = new Bundle();
        args.putInt(PIC, pic);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getInt(PIC) != 0) {
                picId = getArguments().getInt(PIC);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.view_pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView pic = (ImageView) view.findViewById(R.id.pic);
        pic.setImageResource(picId);
    }
}
