package com.cumulations.pockettv;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumulations.shopping.R;

public class TabFragment extends Fragment {

    public TabFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int numberOfColumns = 4;

        RecyclerView gridView = view.findViewById(R.id.gridRecyclerView);
        gridView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        GridRecyclerAdapter gridRecyclerAdapter = new GridRecyclerAdapter(getActivity(), Constants.getTvList());
        gridView.setAdapter(gridRecyclerAdapter);

        DividerItemDecoration verticalDecoration = new DividerItemDecoration(
                gridView.getContext(),
                DividerItemDecoration.HORIZONTAL);
        verticalDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.vertical_divider));
        gridView.addItemDecoration(verticalDecoration);

        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(
                gridView.getContext(),
                DividerItemDecoration.VERTICAL);
        horizontalDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider));
        gridView.addItemDecoration(horizontalDecoration);

//        gridView.addItemDecoration(new GridDividerDecoration(gridView.getContext()));
    }
}